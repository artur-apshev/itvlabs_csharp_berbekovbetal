﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3
{
    public class BubbleSorter
    {
        public static int compare_counter = 0;

        public List<double> Sort(List<double> unsorted_list, bool reverse = false)
        {
            for(int i = 0; i < unsorted_list.Count; i++)
            {
                for(int j = 0; j < unsorted_list.Count; j++)
                {
                    if(unsorted_list[i] < unsorted_list[j])
                    {
                        compare_counter++;

                        var tmp = unsorted_list[i];
                        unsorted_list[i] = unsorted_list[j];
                        unsorted_list[j] = tmp;
                    }
                }
            }

            if (reverse)
                unsorted_list.Reverse();

            return unsorted_list;
        }
    }
}
