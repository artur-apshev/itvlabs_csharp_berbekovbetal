﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labs1
{
    class Program
    {
        static void Main(string[] args)
        {
            int sum = 0;
            bool key = false;
            while(!key)
            {
                Console.WriteLine("Input float number");
                double number;
                key = double.TryParse(Console.ReadLine(), out number);
                if(!key)
                {
                    Console.WriteLine($"Invalid values");
                    continue;
                }
                
                for (int i = 1; i <= 3; ++i)
                {
                    sum += (int)(number * (int)Math.Pow(10, (double)i) % 10);
                }
                Console.WriteLine($"Sum the first 3 numbers is {sum}");
                Console.ReadKey();
            }
        }
    }
}
