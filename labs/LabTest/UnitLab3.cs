﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LabTest
{
    using lab3;
    using System.Collections.Generic;

    [TestClass]
    public class UnitLab3
    {
        private List<double> _unsortedList = new List<double>();

        private void genarateUnsortedList(double range)
        {
            Random rnd = new Random();

            for (double i = 0; i < range; i++)
            {
                _unsortedList.Add(rnd.Next(0, 100));
            }
        }

        [TestMethod]
        public void TestBubble()
        {
            for(double i = 3; i < 4; ++i)
            {
                double range = Math.Pow(10, i);
                genarateUnsortedList(range);

                BubbleSorter b = new BubbleSorter();
                List<double> currentList = b.Sort(new List<double>(_unsortedList));
                _unsortedList.Sort();
                Assert.IsTrue(_unsortedList.SequenceEqual(currentList));
            }
        }

        [TestMethod]
        public void TestMerge()
        {
            for (double i = 3; i < 6; ++i)
            {
                double range = Math.Pow(10, i);
                genarateUnsortedList(range);

                MergeSort m = new MergeSort();
                List<double> currentList = m.Sort(new List<double>(_unsortedList));

                _unsortedList.Sort();
                Assert.IsTrue(_unsortedList.SequenceEqual(currentList));

            }
        }


        [TestMethod]
        public void TestBubbleReverse()
        {
            for (double i = 3; i < 4; ++i)
            {
                double range = Math.Pow(10, i);
                genarateUnsortedList(range);

                BubbleSorter b = new BubbleSorter();
                List<double> currentList = b.Sort(new List<double>(_unsortedList), reverse: true);

                _unsortedList.Sort();
                _unsortedList.Reverse();
                Assert.IsTrue(_unsortedList.SequenceEqual(currentList));
            }
        }
    }
}
